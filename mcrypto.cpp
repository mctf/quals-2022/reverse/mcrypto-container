#include <Windows.h>
#include <stdio.h>
#include <stdint.h>
#include <time.h>
#include <vector>
#include <string>
#include <iostream>

#define VERSION_MAJOR	0x01
#define VERSION_MINOR	0x0c

class File
{
public:
	static bool Exists(std::string const &path)
	{
		FILE* f = fopen(path.c_str(), "r");
		if (f)
		{
			fclose(f);
			return true;
		}
		return false;
	}

	static void Write(std::string const &path, std::vector<uint8_t> &content)
	{
		FILE* f = fopen(path.c_str(), "wb");
		if (f)
		{
			fwrite(&content[0], 1, content.size(), f);
			fclose(f);
			return;
		}
		printf("Unable to create file %s\n", path);
		exit(1);
	}

	static std::vector<uint8_t> Read(std::string const &path)
	{
		FILE* f = fopen(path.c_str(), "rb");
		if (f)
		{
			fseek(f, 0, SEEK_END);
			long sz = ftell(f);
			rewind(f);
			std::vector<uint8_t> rv(sz);
			fread(&rv[0], 1, sz, f);
			fclose(f);
			return rv;
		}
		printf("Unable to read file %s\n", path);
		exit(1);
	}

	static std::string GetName(std::string const &path)
	{
		std::string v = path;
		while (v.find('\\') != std::string::npos)
			v = v.substr(v.find('\\') + 1);
		return v;
	}
};

class Cipher
{
private:
	static void xorBlock(std::vector<uint8_t> &data, size_t offset, std::vector<uint8_t> const &key)
	{
		for (size_t i = 0; i < key.size(); i++)
		{
			size_t index = i + offset;
			if (index >= data.size())
				continue;
			data[index] = data[index] ^ key[i];
		}
	}

	static std::vector<uint8_t> externalCipher(std::vector<uint8_t> const &content, std::vector<uint8_t> const &key)
	{
		std::vector<uint8_t> rv(content);
		size_t length = content.size();
		size_t offset = 0;
		while (length > key.size())
		{
			xorBlock(rv, offset, key);
			length -= key.size();
			offset += key.size();
		}
		if (length != 0)
			xorBlock(rv, offset, key);
		return rv;
	}

	static std::vector<uint8_t> calculateSHA1(std::string const &s)
	{
		HCRYPTPROV hProv = 0;
		if (!CryptAcquireContext(&hProv, nullptr, nullptr, PROV_RSA_FULL, CRYPT_VERIFYCONTEXT))
		{
			printf("Unable to initialize crypto\n");
			exit(1);
		}

		HCRYPTHASH hHash = 0;
		if (!CryptCreateHash(hProv, CALG_SHA1, 0, 0, &hHash))
		{
			printf("Unable to initialize hash\n");
			exit(1);
		}

		if (!CryptHashData(hHash, (LPCBYTE)s.c_str(), s.size(), 0))
		{
			printf("Unable to calculate hash\n");
			exit(1);
		}

		DWORD dwCount = sizeof(DWORD);
		DWORD dwHashLen = 0;
		if (!CryptGetHashParam(hHash, HP_HASHSIZE, (PBYTE)&dwHashLen, &dwCount, 0))
		{
			printf("Unable to get hash size\n");
			exit(1);
		}

		PBYTE value = (PBYTE)malloc(dwHashLen);
		memset(value, 0, dwHashLen);

		if (!CryptGetHashParam(hHash, HP_HASHVAL, value, &dwHashLen, 0))
		{
			printf("Unable to get hash value\n");
			exit(1);
		}

		std::vector<uint8_t> rv(sizeof(value));
		memcpy(&rv[0], value, sizeof(value));

		free(value);
		CryptDestroyHash(hHash);
		CryptReleaseContext(hProv, 0);

		return rv;
	}

public:
	static std::vector<uint8_t> GenerateExternalKey()
	{
		std::vector<uint8_t> key;
		for (uint8_t i = 0; i < 8; i++)
			key.push_back(rand() % 256);
		return key;
	}

	static std::string RequestPassword()
	{
		printf("Enter password: ");
		std::string password;
		getline(std::cin, password);
		return password;
	}

	static std::vector<uint8_t> MakeContainer(std::vector<uint8_t> &content, PCHAR fileName)
	{
		std::string name = File::GetName(fileName);
		std::vector<uint8_t> rv;

		rv.push_back((uint8_t)'M');
		rv.push_back((uint8_t)'C');
		rv.push_back((uint8_t)'C');
		rv.push_back((uint8_t)'1');

		uint32_t contentLength = (uint32_t)content.size();
		rv.push_back((uint8_t)((contentLength >> 16) & 0xff));
		rv.push_back((uint8_t)((contentLength >> 8) & 0xff));
		rv.push_back((uint8_t)(contentLength & 0xff));

		rv.push_back(VERSION_MAJOR);
		rv.push_back(VERSION_MINOR);

		uint16_t nameLength = (uint16_t)name.length();
		rv.push_back((uint8_t)(nameLength & 0xff));

		rv.insert(rv.end(), name.begin(), name.end());
		rv.insert(rv.end(), content.begin(), content.end());

		return rv;
	}

	static std::vector<uint8_t> UnpackContainer(std::vector<uint8_t> &content, std::string* outFilename)
	{
		*outFilename = "";
		if (content.size() < 11)
		{
			return std::vector<uint8_t>(0);
		}
		if ((content[0] != 'M') || (content[1] != 'C') || (content[2] != 'C') || (content[3] != '1'))
		{
			return std::vector<uint8_t>(0);
		}
		uint32_t fileSize = content[4] * 256 * 256 + content[5] * 256 + content[6];
		if ((content[7] != VERSION_MAJOR) || (content[8] != VERSION_MINOR))
		{
			return std::vector<uint8_t>(0);
		}
		uint16_t fileNameSize = content[9];
		if (content.size() != 10 + fileSize + fileNameSize)
		{
			return std::vector<uint8_t>(0);
		}
		(*outFilename) = std::string((const char*)&content[10], fileNameSize);
		std::vector<uint8_t> rv(fileSize);
		memcpy(&rv[0], &content[10 + fileNameSize], fileSize);
		return rv;
	}

	static std::vector<uint8_t> EncryptInternal(std::vector<uint8_t> &content, std::string const &password)
	{
		std::vector<uint8_t> hash = calculateSHA1(password);
		return externalCipher(content, hash);
	}

	static std::vector<uint8_t> DecryptInternal(std::vector<uint8_t> &content, std::string const &password)
	{
		std::vector<uint8_t> hash = calculateSHA1(password);
		return externalCipher(content, hash);
	}

	static std::vector<uint8_t> EncryptExternal(std::vector<uint8_t> &content, std::vector<uint8_t> const &key)
	{
		return externalCipher(content, key);
	}

	static std::vector<uint8_t> DecryptExternal(std::vector<uint8_t> &content, std::vector<uint8_t> const &key)
	{
		return externalCipher(content, key);
	}
};

void printUsage(PCHAR message)
{
	if (message)
		printf("%s\n\n", message);
	printf(
		"MCrypto. Encryption and decryption tool.\n\n"
		"To encrypt file use following command:\n"
		"  mcrypto.exe encrypt <file> <key> <container>\n"
		"\t<file>         file to be encrypted\n"
		"\t<key>          result key-file\n"
		"\t<container>    result container\n\n"
		"To decrypt container use following command:\n"
		"  mcrypto.exe decrypt <container> <key>\n"
		"\t<container>    container to be decrypted\n"
		"\t<key>          key-file for container\n\n"
	);
}

void encryptRoutine(int argc, PCHAR argv[])
{
	if (argc != 5)
	{
		printUsage("Too few arguments for 'encrypt' command.");
		return;
	}
	PCHAR inputFile = argv[2];
	PCHAR keyFile = argv[3];
	PCHAR outputFile = argv[4];
	if (!File::Exists(inputFile))
	{
		printUsage("Input file not found.");
		return;
	}
	std::vector<uint8_t> key = Cipher::GenerateExternalKey();
	std::vector<uint8_t> content = File::Read(inputFile);
	File::Write(keyFile, key);
	std::string password = Cipher::RequestPassword();

	std::vector<uint8_t> internalContent = Cipher::EncryptInternal(content, password);
	std::vector<uint8_t> container = Cipher::MakeContainer(internalContent, inputFile);
	std::vector<uint8_t> externalContent = Cipher::EncryptExternal(container, key);

	File::Write(outputFile, externalContent);
	printf("File successfully encrypted.\n");
}

void decryptRoutine(int argc, PCHAR argv[])
{
	if (argc != 4)
	{
		printUsage("Too few arguments for 'decrypt' command.");
		return;
	}
	PCHAR inputFile = argv[2];
	if (!File::Exists(inputFile))
	{
		printUsage("Input file not found.");
		return;
	}
	PCHAR keyFile = argv[3];
	if (!File::Exists(keyFile))
	{
		printUsage("Key-file not found.");
		return;
	}
	std::vector<uint8_t> content = File::Read(inputFile);
	std::vector<uint8_t> key = File::Read(keyFile);
	if (key.size() != 8)
	{
		printUsage("Key-file not found.");
		return;
	}
	std::string password = Cipher::RequestPassword();

	std::vector<uint8_t> internalContent = Cipher::DecryptExternal(content, key);
	std::string outputFile;
	std::vector<uint8_t> container = Cipher::UnpackContainer(internalContent, &outputFile);
	if ((outputFile.length() == 0) || (container.size() == 0))
	{
		printUsage("Incorrect container");
		return;
	}
	std::vector<uint8_t> outContent = Cipher::DecryptInternal(container, password);

	File::Write(outputFile, outContent);
	printf("File successfully decrypted.\n");
}

int main(int argc, PCHAR argv[])
{
	srand(time(nullptr));
	if (argc < 4)
	{
		printUsage(NULL);
		return 0;
	}
	if (strcmp(argv[1], "encrypt") == 0)
	{
		encryptRoutine(argc, argv);
	}
	else
	{
		if (strcmp(argv[1], "decrypt") == 0)
		{
			decryptRoutine(argc, argv);
		}
		else
		{
			printUsage("Unknown command.");
		}
	}
	return 0;
}

